import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);

//实例化vuex --5大核心
const store = new Vuex.Store({
  state: {
    token: "afa79f7adf7adf7ad9f7as9df",
  },
  actions: {
    setToken({ commit }, v) {
      console.log("actions--setToken方法", v);
      commit("SETTOKEN", v);
    },
  },
  mutations: {
    SETTOKEN(state, v) {
      state.token = v;
    },
  },
  getters: {},
  modules: {},
});

//抛出store
export default store;
