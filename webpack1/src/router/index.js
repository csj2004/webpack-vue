import VueRouter from "vue-router";
import Vue from "vue";

Vue.use(VueRouter);

import about from "../views/about.vue";
import home from "../views/home.vue";

//配置路由
const routes = [
  {
    path: "/",
    name: "home",
    component: home,
  },
  {
    path: "/about",
    name: "about",
    component: about,
  },
];

//实例化路由
const router = new VueRouter({
  routes,
});

//抛出路由
export default router;
