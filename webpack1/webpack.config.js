const path = require("path");
// 引入抽离css
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const htmlwebpackplugin = require("html-webpack-plugin");
//引入 VueLoaderPlugin
const { VueLoaderPlugin } = require("vue-loader");
const config = {
  // 模式
  mode:"production",
  // 入口
  entry: "./src/main.js",
  // 出口
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "main.js",
  },
  module: {
    rules: [
      { test: /\.vue$/, use: ["vue-loader"] },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // 将 JS 字符串生成为 style 节点
          MiniCssExtractPlugin.loader,
          // 将 CSS 转化成 CommonJS 模块
          "css-loader",
          // 将 Sass 编译成 CSS
          "sass-loader",
        ],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: "asset/resource",
      },
    ],
  },
  devServer: {
    //webpack server在内存中运行，与本地打包的dist无实际关联
    //指定运行目录
    static: "./dist",
    //指定运行端口号
    port: 8888,
  },
  resolve: {
    extensions: [".js", ".json", ".vue"],
  },
  performance: {
    // 关闭性能提示
    hints: false,
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "css/index.css",
    }),
    new htmlwebpackplugin({
     template:"./public/index.html"
    }),
    new VueLoaderPlugin(),
  ],
};
module.exports = config;
