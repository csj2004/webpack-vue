import React from "react";
import ReactDOM from "react-dom/client";
// 引入样式重置
import "normalize.css/normalize.css";
import store from "./store";
import { Provider } from "react-redux";
import "../src/style/appstyle/app.min.css";
import "../src/style/indexstyle/index.min.css";
import "../src/style/catestyle/cate.min.css";
// 引入vant样式
import "react-vant/es/styles";
import reportWebVitals from "./reportWebVitals";
import { RouterProvider } from "react-router-dom";

// 引入路由
import router from "./router/index";
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router}></RouterProvider>
    </Provider>
  </React.StrictMode>
);

reportWebVitals();
