import React, { Component } from "react";
import { NavBar, Toast } from "react-vant";
import { searchapi, goodsdetailapi } from "../utils/api";
import { DeleteO, Cross, Arrow } from "@react-vant/icons";
class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: "",
      goodslist: [],
      keys: [],
      isshow: false,
    };
  }
  componentDidMount = () => {
    let { keys } = this.state;
    if (localStorage.getItem("keys")) {
      keys = JSON.parse(localStorage.getItem("keys"));
      this.setState({
        keys,
      });
    } else {
      this.setState({
        keys: [],
      });
    }
  };
  changekey = (e) => {
    let name = e.target.value;
    this.setState({
      key: name,
    });
  };
  back() {
    window.history.back();
  }
  search = (e) => {
    let { key, goodslist, keys, isshow } = this.state;
    if (e.keyCode === 13) {
      if (key === "") {
        Toast("不能为空");
        return false;
      }
      isshow = true;
      this.setState({
        isshow,
      });
      let flag = keys.some((ele) => {
        return ele === key;
      });
      if (!flag) {
        keys.unshift(key);
        // console.log(keys);
        localStorage.setItem("keys", JSON.stringify(keys));
        this.setState({
          keys,
        });
      } else {
        Toast("搜索过此类");
      }
      searchapi(key).then((res) => {
        console.log(res.message.goods);
        goodslist = res.message.goods;
        this.setState({
          goodslist,
        });
      });
    }
  };
  delh() {
    let { keys } = this.state;
    keys = [];
    localStorage.setItem("keys", JSON.stringify(keys));
    this.setState({
      keys,
    });
  }
  clearkey() {
    let { key, isshow } = this.state;
    key = "";
    isshow = false;
    this.setState({
      key,
      isshow,
    });
  }
  // 点击跳转思昂品详情，传id并发送请求进行渲染画面
  todetail(id) {
    goodsdetailapi(id).then((res) => {
      console.log(res);
    });
  }
  // 点击搜索历史复查跳转到商品列表，传query值到商品列表并发送请求进行渲染画面
  togoodslist = (val) => {
    window.location.href = "/goodslist?state=0&query=" + encodeURI(val);
    // let { key, goodslist, isshow } = this.state;
    // key = val;
    // isshow = true;
    // this.setState({
    //   key: key,
    //   isshow: isshow,
    // });
    // searchapi({ query: val, id: "" }).then((res) => {
    //   // console.log(res.message.goods);
    //   goodslist = res.message.goods;
    //   this.setState({
    //     goodslist,
    //   });
    // });
  };
  render() {
    let { keys, isshow, goodslist } = this.state;
    return (
      <div className="search">
        <NavBar
          title="搜索"
          leftText="返回"
          onClickLeft={this.back.bind(this)}
        />
        <div className="ipt">
          <input
            type="text"
            value={this.state.key}
            onChange={this.changekey}
            onKeyDown={this.search.bind(this)}
            placeholder="请输入"
          />
          <span className="close" onClick={this.clearkey.bind(this)}>
            <Cross />
          </span>
        </div>
        <div className={`${isshow === false ? "" : "showhistory"} history`}>
          <div className="h-top">
            <span>搜索历史</span>
            <span onClick={this.delh.bind(this)}>
              <DeleteO />
            </span>
          </div>
          <div className="h-list">
            {keys.map((ele, i) => {
              return (
                <span
                  className="h-list-item"
                  onClick={this.togoodslist.bind(this, ele)}
                  key={i}
                >
                  {ele}
                </span>
              );
            })}
          </div>
        </div>
        <div className={`${isshow === true ? "" : "showhistory"} searchlist`}>
          {goodslist.map((ele) => {
            return (
              <div
                className="goodsitem"
                onClick={this.todetail.bind(this, ele.goods_id)}
                key={ele.goods_id}
              >
                <div>{ele.goods_name}</div>
                <div>
                  <Arrow />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Search;
