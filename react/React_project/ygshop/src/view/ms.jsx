import React, { useState } from "react";
import Csg from "./csg";
import { Link, NavLink, useNavigate } from "react-router-dom";
const Ms = (props) => {
  const router = useNavigate();
  const getz = (val) => {
    console.log(val);
  };
  const tomyp = () => {
    // 编程式跳转引入usenavigate然后声明变量接收方法接收参数
    //state  接收使用useLocation然后声明变量接收方法接收参数
    router("/myp", { state: { name: 1, age: 2, arr: [1, 2, 3, 4, 5] } });
    // search   传参不需要在路由上配置 ，直接在跳转的路径拼接
    // 接受的页面引入usesearchparams 生命变量接收方法（变量为【search】）然后search中的get方法获取get('属性名')
    // router("/myp?id=666");
  };
  const [state, setstate] = useState({
    id: 1,
  });
  return (
    <div>
      秒杀
      {/* 声明式跳转传参   跳转的页面引入useparams接收 */}
      <Link to={`/csg/${state.id}`}>toscg</Link>
      <NavLink to={"/myp"}>tomyp</NavLink>
      <Csg msg="哈哈哈" getz={getz}>
        111
      </Csg>
      <button onClick={tomyp}>跳转myp</button>
    </div>
  );
};

export default Ms;
