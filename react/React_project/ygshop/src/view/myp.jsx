import React, { useState, useEffect, useMemo } from "react";
import { useLocation, useSearchParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { increment, decrement } from "../store/counter";
const Myp = (props) => {
  const count = useSelector((state) => state.counterReducer.value);
  const dispatch = useDispatch();
  const params = useLocation();
  console.log(params);
  // const [search] = useSearchParams();
  useEffect(() => {
    // console.log(params);
    // console.log(search.get("id"));
    return () => {};
  }, []);
  const [state, setstate] = useState({
    name: "母婴品",
  });
  const changename = () => {
    setstate({
      name: "哈哈哈哈哈",
    });
  };
  let sum = useMemo(() => {
    return 1;
  }, []);
  return (
    <div onClick={changename}>
      {state.name}
      {sum}
      <p>{count}</p>
      <div className="btn">
        <button onClick={() => dispatch(increment())}>+</button>
        <button
          onClick={() => dispatch(decrement())}
          style={{ marginLeft: 20 }}
        >
         -
        </button>
      </div>
    </div>
    
  );
};

export default Myp;
