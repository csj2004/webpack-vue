import React, { Component } from "react";
import { Search } from "@react-vant/icons";
import { cateapi } from "../utils/api";
class Cate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      catelist: [],
      flag: 1,
      cateindex: 0,
      catedetail: [],
    };
  }
  componentDidMount() {
    this.getcatelist();
  }
  getcatelist() {
    let { catelist, catedetail, cateindex } = this.state;
    cateapi().then((res) => {
      // console.log(res.message);
      catelist = res.message;
      catedetail = catelist[cateindex].children;
      this.setState({
        catelist,
        catedetail,
      });
    });
  }
  tosearch() {
    window.location.href = "/search";
  }
  changenav(id, i) {
    let { flag, cateindex, catedetail, catelist } = this.state;
    flag = id;
    let a = catelist.findIndex((ele, i) => {
      return ele.cat_id === id;
    });
    cateindex = a;
    catedetail = JSON.parse(JSON.stringify(catelist[cateindex].children));
    this.setState({
      flag,
      cateindex,
      catedetail,
    });
  }
  togoodslist(id) {
    window.location.href = "/goodslist?state=1&id=" + id;
  }
  render() {
    let { catelist, flag, catedetail } = this.state;
    return (
      <div className="cate">
        <div className="top">
          <div className="title">商城分类</div>
          <div className="ipt" onClick={this.tosearch.bind(this)}>
            <span>
              <Search />
              搜索
            </span>
          </div>
        </div>
        <div className="content">
          <div className="left">
            {catelist.map((ele, i) => {
              return (
                <div
                  className={`${
                    flag === ele.cat_id ? "catenavchange" : ""
                  } items`}
                  onClick={this.changenav.bind(this, ele.cat_id, i)}
                  key={ele.cat_id}
                >
                  {ele.cat_name}
                </div>
              );
            })}
          </div>
          <div className="right">
            {catedetail.length > 0 ? (
              <div>
                {catedetail.map((ele, i) => {
                  return (
                    <div className="r-items" key={ele.cat_id}>
                      <h5>/{ele.cat_name}/</h5>
                      <div>
                        {ele.children ? (
                          <div className="catealllist">
                            {ele.children.map((item, index) => {
                              return (
                                <div
                                  className="r-item-catelist"
                                  onClick={this.togoodslist.bind(
                                    this,
                                    item.cat_id
                                  )}
                                  key={item.cat_id}
                                >
                                  <img src={item.cat_icon} alt="" />
                                  <div>{item.cat_name}</div>
                                </div>
                              );
                            })}
                          </div>
                        ) : (
                          <div className="zw">暂无数据</div>
                        )}
                      </div>
                    </div>
                  );
                })}
              </div>
            ) : (
              <div className="zw">暂无数据</div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Cate;
