import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { goodsdetailapi } from "../utils/api";
const Goodsdetail = () => {
  const [goodsmessage, setgoodsmessage] = useState({});
  const params = useLocation();
  useEffect(() => {
    // console.log(params.state.id);
    let id = params.state.id;
    goodsdetailapi(id).then((res) => {
      //   console.log(res.message);
      setgoodsmessage(res.message);
    });
    return () => {
    };
  }, []);
  return (
    <div
      dangerouslySetInnerHTML={{ __html: goodsmessage.goods_introduce }}
    ></div>
  );
};

export default Goodsdetail;
