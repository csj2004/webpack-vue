import React, { useEffect, useState } from "react";
import getQuery from "../utils/geturlquery";
import { searchapi, searchapi1 } from "../utils/api";
import { NavBar } from "react-vant";
import { useNavigate } from "react-router-dom";
const Goodslist = () => {
  const router = useNavigate();
  const [goodslist, setgoodslist] = useState([]);
  useEffect(() => {
    // console.log(window.location.href);
    let url = window.location.href;
    // console.log(decodeURI(getQuery(url)["query"]));
    let obj = getQuery(url);
    // console.log(obj);
    if (obj.state === "0") {
      searchapi(decodeURI(obj.query)).then((res) => {
        // console.log(res.message.goods);
        setgoodslist([...res.message.goods]);
      });
    } else {
      searchapi1(obj.id).then((res) => {
        // console.log(res.message.goods);
        setgoodslist([...res.message.goods]);
      });
    }
    return () => {};
  }, []);
  const back = () => {
    window.history.back();
  };
  const togoodsdetail = (id) => {
    router("/goodsdetail", { state: { id: id } });
  };
  return (
    <div className="goodslist">
      <NavBar title="商品列表" leftText="返回" onClickLeft={back} />
      {/* 商品列表 */}
      <ul>
        {goodslist.map((ele, index) => {
          return (
            <li
              className="goodsitems"
              onClick={togoodsdetail.bind(this, ele.goods_id)}
              key={ele.goods_id}
            >
              <div className="l">
                <img src={ele.goods_small_logo} alt="" />
              </div>
              <div className="r">
                <div>{ele.goods_name}</div>
                <div>￥{ele.goods_price}</div>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Goodslist;
