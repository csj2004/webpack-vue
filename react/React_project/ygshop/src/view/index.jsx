import React, { Component } from "react";
import { getbannerapi, getnavapi, getfloorapi } from "../utils/api";
import { Search } from "@react-vant/icons";
import { Swiper } from "react-vant";
class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bannerlist: [],
      navlist: [],
      floor: [],
    };
  }
  componentDidMount() {
    this.getbannerlist();
    this.getnavlist();
    this.getfloorlist();
  }
  getbannerlist = () => {
    let { bannerlist } = this.state;
    getbannerapi().then((res) => {
      //   console.log(res.message);
      bannerlist = res.message;
      this.setState({
        bannerlist,
      });
    });
  };
  getnavlist = () => {
    let { navlist } = this.state;
    getnavapi().then((res) => {
      //   console.log(res.message);
      navlist = res.message;
      this.setState({
        navlist,
      });
    });
  };
  getfloorlist = () => {
    let { floor } = this.state;
    getfloorapi().then((res) => {
      //   console.log(res.message);
      floor = res.message;
      this.setState({
        floor,
      });
    });
  };
  tosearch() {
    window.location.href = "/search";
  }
  changepage(i) {
    if (i === 0) {
      window.location.href = "/cate";
    } else if (i === 1) {
      window.location.href = "/ms";
    } else if (i === 2) {
      window.location.href = "/csg";
    } else {
      window.location.href = "/myp";
    }
  }
  render() {
    const { bannerlist, navlist, floor } = this.state;
    return (
      <div className="index">
        {/* 头部 */}
        <div className="top">
          <div className="title">优购商城</div>
          <div className="ipt" onClick={this.tosearch.bind(this)}>
            <span>
              <Search />
              搜索
            </span>
          </div>
        </div>
        {/* 轮播图 */}
        <div className="demo-swiper">
          <Swiper autoplay={5000} loop>
            {bannerlist.map((ele) => (
              <Swiper.Item key={ele.goods_id}>
                <img alt="" src={ele.image_src} />
              </Swiper.Item>
            ))}
          </Swiper>
        </div>
        {/* nav栏 */}
        <div className="navitem">
          {navlist.map((ele, i) => {
            return (
              <div
                className="item"
                key={i}
                onClick={this.changepage.bind(this, i)}
              >
                <img src={ele.image_src} alt="" />
              </div>
            );
          })}
        </div>
        {/* 楼层 */}
        <div className="floor">
          {floor.map((ele, i) => {
            return (
              <div className="flooritem" key={i}>
                <div className="title">
                  <img src={ele.floor_title.image_src} alt="" />
                </div>
                <div className="list">
                  {ele.product_list.map((item, is) => {
                    return (
                      <div className="listitem" key={is}>
                        <img src={item.image_src} alt="" />
                      </div>
                    );
                  })}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Index;
