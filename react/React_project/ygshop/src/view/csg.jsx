import React, { useReducer, useEffect } from "react";
import { useParams } from "react-router-dom";
const obj = {
  num: 1,
};
function reducer(state, action) {
  switch (action.type) {
    case "jia":
      return { num: state.num + 1 };
    case "jian":
      return { num: state.num - 1 };
    default:
      throw new Error();
  }
}
const Csg = (props) => {
  const [state, dispath] = useReducer(reducer, obj);
  const zcf = () => {
    props.getz("嘻嘻");
  };
  const params = useParams();
  useEffect(() => {
    console.log(params);
    return () => {};
  }, []);
  return (
    <div>
      <h2>计数器组件{state.num}</h2>
      <button onClick={() => dispath({ type: "jia" })}>递增</button>
      <button onClick={() => dispath({ type: "jian" })}>递减</button>
      超市购
      {/* 匿名插槽，父组件中子组件标签内的数据在子组件中用props.children */}
      {/* 具名插槽使用父传子的数据 */}
      <div>{props.children}</div>
      <div onClick={zcf}>父传子的msg==={props.msg}</div>
    </div>
  );
};

export default Csg;
