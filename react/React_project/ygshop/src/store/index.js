import { configureStore } from "@reduxjs/toolkit";
//引入计数counter reducer
import counterReducer from "./counter";

export default configureStore({
  //相当于vuex中的module
  reducer: {
    counterReducer,
  },
});
