import { createSlice } from "@reduxjs/toolkit";
export const counterSlice = createSlice({
  //当前模块名
  name: "counter",
  //相当于vuex上的state,数据源
  initialState: {
    value: 0,
    // token: "afdda0df8a-d0f",
    // useInfo: {
    //   name: "admin",
    //   pw: "123456",
    // },
  },
  //相当于vuex中的mutation
  reducers: {
    //加方法
    increment: (state) => {
      state.value += 1;
    },
    //减方法
    decrement: (state) => {
      state.value -= 1;
    },
    //按传参的值进行递增或递减
    incrementByAmount: (state, action) => {
      state.value += action.payload;
    },
  },
});

//导出对应的actions
export const { increment, decrement, incrementByAmount } = counterSlice.actions;

//导出reducer
export default counterSlice.reducer;
