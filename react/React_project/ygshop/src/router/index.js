import { createBrowserRouter } from "react-router-dom";
import Index from "../view/index";
import Cate from "../view/cate";
import Car from "../view/car";
import My from "../view/my";
import App from "../App";
import Search from "../view/search";
import Ms from "../view/ms";
import Csg from "../view/csg";
import Myp from "../view/myp";
import Goodslist from "../view/goodslist";
import Goodsdetail from "../view/goodsdetail";
const router = createBrowserRouter([
  {
    path: "/",
    element: <App></App>,
    children: [
      {
        // path: "/index",
        index: true,
        element: <Index></Index>,
      },
      {
        path: "/cate",
        element: <Cate></Cate>,
      },
      {
        path: "/car",
        element: <Car></Car>,
      },
      {
        path: "/my",
        element: <My></My>,
      },
    ],
  },
  {
    path: "/search",
    element: <Search></Search>,
  },
  {
    path: "/ms",
    element: <Ms></Ms>,
  },
  {
    path: "/csg/:id",
    element: <Csg></Csg>,
  },
  {
    path: "/myp",
    element: <Myp></Myp>,
  },
  {
    path: "/goodslist",
    element: <Goodslist></Goodslist>,
  },
  {
    path: "/goodsdetail",
    element: <Goodsdetail></Goodsdetail>,
  },
]);
export default router;
