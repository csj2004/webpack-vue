import React, { Component } from "react";
import { Outlet, Link } from "react-router-dom";
import { Tabbar } from "react-vant";
import { ShoppingCartO, HomeO, AppsO, UserO } from "@react-vant/icons";
class App extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <div className="app">
        <Outlet></Outlet>
        <div className="demo-tabbar">
          <Tabbar activeColor="#bf0101" inactiveColor="#03191b">
            <Tabbar.Item icon={<HomeO />}>
              <Link className="a" to={"/"}>
                首页
              </Link>
            </Tabbar.Item>
            <Tabbar.Item icon={<AppsO />}>
              <Link className="a" to={"/cate"}>
                分类
              </Link>
            </Tabbar.Item>
            <Tabbar.Item icon={<ShoppingCartO />}>
              <Link className="a" to={"/car"}>
                购物车
              </Link>
            </Tabbar.Item>
            <Tabbar.Item icon={<UserO />}>
              <Link className="a" to={"/my"}>
                我的
              </Link>
            </Tabbar.Item>
          </Tabbar>
        </div>
      </div>
    );
  }
}

export default App;
