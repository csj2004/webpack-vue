// var url = "http://www.baidu.com?name=1&age=9&like=vue";
function getQuery(str) {
  //str.split('?')  =>   ['http://www.baidu.com','name=1&age=9&like=vue']  第一次分割
  //str.split('?')[1].split('&')  =>['name=1','age=9','like=vue']          第二次分割

  let arr = str.split("?")[1].split("&");
  let obj = {};
  arr.forEach((item) => {
    obj[item.split("=")[0]] = item.split("=")[1]; //['name','1'] ['age','9'] ['like',vue] 第三次分割
  });
  return obj;
}
// console.log(getQuery(url));
// getQuery(url);
export default getQuery;
