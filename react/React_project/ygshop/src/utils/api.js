import instance from "./http";
// 轮播
export const getbannerapi = async () => {
  let { data } = await instance.get("public/v1/home/swiperdata");
  return data;
};
// 导航
export const getnavapi = async () => {
  let { data } = await instance.get("public/v1/home/catitems");
  return data;
};
// 楼层
export const getfloorapi = async () => {
  let { data } = await instance.get("public/v1/home/floordata");
  return data;
};
// 搜索1
export const searchapi = async (query) => {
  let { data } = await instance.get(
    `public/v1/goods/search?query=${query}`
  );
  return data;
};
// 搜索2
export const searchapi1 = async (id) => {
  let { data } = await instance.get(
    `public/v1/goods/search?cid=${id}`
  );
  return data;
};
// 商品详情
export const goodsdetailapi = async (obj) => {
  let { data } = await instance.get(`public/v1/goods/detail?goods_id=${obj}`);
  return data;
};
// 分类
export const cateapi = async () => {
  let { data } = await instance.get(`public/v1/categories`);
  return data;
};
