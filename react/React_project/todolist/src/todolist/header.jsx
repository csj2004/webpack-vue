import React, { Component } from "react";

class Header extends Component {
  constructor(props) {
    super(props);
    // 为input创建ref
    this.ipt = React.createRef();
  }
  add(e, isclick) {
    // console.log(e);
    if (e.keyCode === 13 || isclick === true) {
    //   console.log(this.ipt.current.value);
      let obj = {
        name: this.ipt.current.value,
        id: new Date().getTime(),
        status: false,
      };
      this.props.callback(obj);
      this.ipt.current.value = "";
    }
  }
  render() {
    return (
      <div className="header">
        <input
          type="text"
          ref={this.ipt}
          onKeyUp={(e) => this.add(e)}
          placeholder="请输入任务"
        />
        <button onClick={(e) => this.add(e, true)}>提交</button>
      </div>
    );
  }
}

export default Header;
