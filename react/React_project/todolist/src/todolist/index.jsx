import React, { Component } from "react";
// 引入各个组件
import Header from "./header";
import Listitem from "./listitem";
import Footer from "./footer";
// 引入todolist整体样式
import "./style/inde.min.css";
class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      synum: 0,
      flags: false,
    };
  }
  componentDidMount() {
    // console.log(localStorage.getItem("list"));
    if (localStorage.getItem("list")) {
      let obj = JSON.parse(localStorage.getItem("list"));
      this.setState({
        list: obj,
      });
    } else {
      this.setState({
        list: [],
      });
    }
  }
  // 接收ipt内容
  callback(val) {
    if (val.name === "") {
      console.log("内容不能为空");
      return;
    }
    let { flags } = this.state;
    this.state.list.push(val);
    flags = false;
    this.setState({
      list: this.state.list,
      flags,
    });
    localStorage.setItem("list", JSON.stringify(this.state.list));
  }
  //   切换单个状态
  changestatus(val) {
    let { list, flags } = this.state;
    // console.log(val);
    let index = this.findindex(val);
    // console.log(index);
    list[index].status = !list[index].status;
    this.setState({
      list: this.state.list,
    });
    localStorage.setItem("list", JSON.stringify(list));
    let flag = list.every((ele) => {
      return ele.status === true;
    });
    // console.log(flag);
    flags = flag;
    this.setState({
      flags,
    });
  }
  //   查找下标的函数
  findindex(id) {
    return this.state.list.findIndex((ele, index) => {
      return id === ele.id;
    });
  }
  //   删除
  del(val) {
    let { list, flags } = this.state;
    let index = this.findindex(val);
    list.splice(index, 1);
    if (list.length === 0) {
      flags = false;
    } else {
      var flag = list.every((ele) => {
        return ele.status === true;
      });
      flags = flag;
    }
    this.setState({
      list,
      flags,
    });
    localStorage.setItem("list", JSON.stringify(list));
  }
  //   删除选中
  dels() {
    let { list, flags } = this.state;
    list = list.filter((ele) => {
      return ele.status === false;
    });
    if (list.length === 0) {
      flags = false;
    } else {
      var flag = list.every((ele) => {
        return ele.status === true;
      });
      flags = flag;
    }
    // console.log(list);
    this.setState({
      list,
      flags,
    });
    localStorage.setItem("list", list);
  }
  changeallstate() {
    let { flags, list } = this.state;
    flags = !flags;
    list.forEach((ele) => {
      ele.status = flags;
    });
    this.setState({
      list,
      flags,
    });
  }
  render() {
    return (
      <div className="box">
        <div className="todolist">
          <h1>TODOLIST</h1>
          <Header callback={this.callback.bind(this)}></Header>
          <Listitem
            lists={this.state.list}
            changestatus={this.changestatus.bind(this)}
            del={this.del.bind(this)}
          ></Listitem>
          <Footer
            flags={this.state.flags}
            lists={this.state.list}
            // changeall={this.changeall.bind(this)}
            changeallstate={this.changeallstate.bind(this)}
            dels={this.dels.bind(this)}
          ></Footer>
        </div>
      </div>
    );
  }
}

export default Index;
