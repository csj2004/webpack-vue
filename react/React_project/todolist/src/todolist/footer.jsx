import React, { Component } from "react";

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  changeallstate() {
    this.props.changeallstate();
  }
  dels() {
    this.props.dels();
  }
  render() {
    return (
      <div className="footer">
        <div>
          <input
            type="checkbox"
            checked={this.props.flags}
            onChange={this.changeallstate.bind(this)}
          />
          <span>全选</span>
        </div>
        <div>
          <span>全部：{this.props.lists.length}个</span>
        </div>
        <div>
          <span>
            剩余：
            {
              this.props.lists.filter((ele) => {
                return ele.status === false;
              }).length
            }
          </span>
        </div>
        <div>
          <button onClick={this.dels.bind(this)}>删除选中</button>
        </div>
      </div>
    );
  }
}

export default Footer;
