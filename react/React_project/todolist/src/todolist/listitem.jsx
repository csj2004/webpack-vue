import React, { Component } from "react";

class Listitem extends Component {
  changestatus(id) {
    // console.log(id);
    this.props.changestatus(id);
  }
  del(id) {
    this.props.del(id);
  }
  render() {
    const { lists } = this.props;
    if (lists.length !== 0) {
      return (
        <div className="listitem">
          <ul>
            {lists.map((ele, index) => {
              return (
                <li key={ele.id}>
                  <div>
                    <input
                      type="checkbox"
                      checked={ele.status}
                      onChange={this.changestatus.bind(this, ele.id)}
                    />
                    <span>{ele.name}</span>
                  </div>
                  <div>
                    <button onClick={this.del.bind(this, ele.id)}>删除</button>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      );
    } else {
      return <h1>暂无数据</h1>;
    }
  }
}

export default Listitem;
