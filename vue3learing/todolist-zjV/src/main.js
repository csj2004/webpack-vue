import { createApp } from "vue";
import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import App from "./App.vue";
import router from "./router";
// 重置默认样式
import "reset-css";
//引入miit
import mitt from "mitt";
const app = createApp(App);
// app.use(createPinia());
//将mitt挂载到app全局上   实现类似Vue.prototype.$http=axios
app.config.globalProperties.$bus = mitt();
const store = createPinia();
store.use(piniaPluginPersistedstate);
app.use(store);
app.use(router);
app.mount("#app");
