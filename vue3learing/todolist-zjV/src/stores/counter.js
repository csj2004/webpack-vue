import { defineStore } from "pinia";
export const useCounterStore = defineStore("counter", {
  persist: true,
  state: () => ({
    testarr: [],
  }),
  actions: {
    add(testarr, obj) {
      testarr.push(obj);
      // console.log(testarr, obj);
    },
    del(testarr, id) {
      testarr.forEach((ele, i) => {
        if (ele.id == id) {
          testarr.splice(i, 1);
        }
      });
    },
    changestate(testarr, id) {
      testarr.forEach((ele, i) => {
        if (ele.id == id) {
          testarr[i].status = !testarr[i].status;
        }
      });
    },
  },
  getters: {
    sy(state) {
      var num = 0;
      state.testarr.forEach((ele) => {
        if (ele.status == false) {
          num++;
        }
      });
      return num;
    },
  },
});
