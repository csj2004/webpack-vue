import App from "./App.vue";
import { createApp } from "vue";
import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import "amfe-flexible";
import router from "./router";
import imgstyle from "@/assets/imgstyle.css";
// 2. 引入组件样式
import "vant/lib/index.css";
// import "./assets/main.css";
import "normalize.css";

const app = createApp(App);
const store = createPinia();
store.use(piniaPluginPersistedstate);
app.use(store);
app.use(router);
app.mount("#app");
