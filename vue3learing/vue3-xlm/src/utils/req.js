import instance from "./http";
export const getlessonapi = async function () {
  let data = await instance.get("home/getFirstClassify");
  return data;
};
export const getlessondetailapi = async function (id) {
  let data = await instance.get(`home/getMajor/${id}`);
  return data;
};
export const tablistapi = async function (id) {
  let data = await instance.get(`home/getSecondClassify/${id}`);
  return data;
};
export const bannerlistapi = async function (id) {
  let data = await instance.get(`home/banner/${id}/4`);
  return data;
};
export const ggapi = async function (id) {
  let data = await instance.get(`home/liveToday/${id}`);
  return data;
};
export const lessonapilist = async function (id) {
  let data = await instance.get(`home/marketingCourse/${id}`);
  return data;
};
export const lessondetailapi = async function (id, st = 0) {
  let data = await instance.get(`courseInfo/basis_id=${id}/st=${st}`);
  return data;
};
export const kqzbapi = async function (id) {
  let data = await instance.get(`getWechatInfo/:id=1442`);
  return data;
};
export const yzmapi = async function () {
  let data = await instance.get(`getImgCode`);
  return data;
};
export const sjyzmapi = async function (obj) {
  let data = await instance.post(`getsmscode`, {
    mobile: obj.mobile,
    sms_type: "login",
  });
  return data;
};
export const searchapi = async function (obj) {
  let data = await instance.get(
    `home/classifyCourse/1?%E5%90%95&classify_id=${obj.id}&title=${obj.title}`
  );
  return data;
};
export const passwordokapi = async function (obj) {
  let data = await instance.post(`getpassword`, {
    mobile: obj.mobile,
    sms_code: obj.sms_code,
    user_pass: obj.user_pass,
  });
  return data;
};
export const loginapi = async function (obj) {
  let data = await instance.post(`login`, {
    captcha: obj.captcha,
    device_id: 1,
    device_type: 2,
    key: obj.key,
    mobile: obj.mobile,
    sms_code: obj.sms_code,
    user_pass: obj.password,
  });
  return data;
};
export const likeapi = async function (obj) {
  let data = await instance.post("collect", {
    basis_id: obj.id,
    type: 1,
    token: obj.token,
  });
  return data;
};
export const showownapi = async function (id) {
  let data = await instance.get(`showone/id=${id}?id=${id}`);
  return data;
};
export const likelistapi = async function () {
  let data = await instance.get(`collect`);
  return data;
};
export const aboutusapi = async function () {
  let data = await instance.get(`about/type=1`);
  return data;
};
export const noticeapi = async function (id) {
  let data = await instance.get(`msgNotice?is_msg_notice=${id}`);
  return data;
};
export const caresjyzmapi = async function (obj) {
  let data = await instance.post(`getsmscode`, {
    mobile: obj,
    sms_type: "editmobile",
  });
  return data;
};
export const changetel = async function (obj) {
  let data = await instance.post(`modifyMobile`, {
    current_mobile: obj.current_mobile,
    new_mobile: obj.new_mobile,
    sms_code: obj.sms_code,
    sms_type: "editmobile",
  });
  return data;
};
export const updateInfoapi = async function (obj) {
  let data = await instance.put(`updateInfo`, {
    ...obj,
  });
  return data;
};
export const getschoolapi = async function (obj) {
  let data = await instance.post(`study/getCollege`, {
    page: 1,
    page_count: 20,
    keyword: obj,
  });
  return data;
};
