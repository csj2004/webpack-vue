import axios from "axios";
import { closeToast, showLoadingToast } from "vant";
import router from "@/router";
const instance = axios.create({
  baseURL: "https://www.lexuemiao.com/api/app/",
  timeout: 1000,
});
// 白名单
const whiteList = [
  "/collect",
  "getCollege",
  "/showone",
  "/updateInfo",
  "/passwordLogin",
  "/personalCenter",
  "/setPassword",
];
// 添加请求拦截器
instance.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    showLoadingToast({
      message: "加载中...",
      forbidClick: true,
    });
    const token = localStorage.getItem("token");
    //哪些页面需求token,配置白名单
    if (whiteList.includes(config.url)) {
      if (!token) {
        //重定向到登录
        router.replace("/login");
      }
    }
    //否则配置请求头，携带token
    config.headers["zywxtoken"] = "zywx" + token;
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
instance.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    // console.log(response);
    if (response.data.code == 200) {
      closeToast();
    }
    return response;
  },
  function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);
export default instance;
