import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/index",
      name: "index",
      redirect: "/",
      component: () => import("../views/index.vue"),
      children: [
        {
          path: "/",
          name: "home",
          component: HomeView,
        },
        {
          path: "/mylesson",
          name: "mylesson",
          component: () => import("../views/mylesson.vue"),
        },
        {
          path: "/mw",
          name: "mw",
          component: () => import("../views/mw.vue"),
        },
        {
          path: "/utils",
          name: "utils",
          component: () => import("../views/utils.vue"),
        },
        {
          path: "/my",
          name: "my",
          meta: {
            auth: true,
          },
          component: () => import("../views/my.vue"),
        },
      ],
    },
    {
      path: "/about",
      name: "about",
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/login.vue"),
    },
    {
      path: "/passwordok",
      name: "passwordok",
      component: () => import("../views/passwordok.vue"),
    },
    {
      path: "/search",
      name: "search",
      component: () => import("../views/search.vue"),
    },
    {
      path: "/seting",
      name: "seting",
      component: () => import("../views/seting.vue"),
    },
    {
      path: "/aboutus",
      name: "aboutus",
      component: () => import("../views/aboutus.vue"),
    },
    {
      path: "/care",
      name: "care",
      component: () => import("../views/care.vue"),
    },
    {
      path: "/changemobile",
      name: "changemobile",
      component: () => import("../views/changemobile.vue"),
    },
    {
      path: "/changepassword",
      name: "changepassword",
      component: () => import("../views/changepassword.vue"),
    },
    {
      path: "/userinfo",
      name: "userinfo",
      component: () => import("../views/userinfo.vue"),
    },
    {
      path: "/changename",
      name: "changename",
      component: () => import("../views/changename.vue"),
    },
    {
      path: "/changesex",
      name: "changesex",
      component: () => import("../views/changesex.vue"),
    },
    {
      path: "/changezy",
      name: "changezy",
      component: () => import("../views/changezy.vue"),
    },
    {
      path: "/changeschool",
      name: "changeschool",
      component: () => import("../views/changeschool.vue"),
    },
  ],
});
router.beforeEach((to, form, next) => {
  const flag = to.matched.some((item) => {
    return item.meta.auth;
  });
  if (to.fullPath !== "/login") {
    if (flag) {
      const token = localStorage.getItem("token");
      if (!token) {
        return next({
          path: "/login",
        });
      }
      return next();
    }
  }
  next();
});
export default router;
